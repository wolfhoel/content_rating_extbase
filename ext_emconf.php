<?php

/*
 * Extension Manager/Repository config file for ext "content_rating_extbase".
 *
 * Auto generated 05-03-2018 23:20
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Content Rating Extbase',
    'description' => 'Extbase version of content_rating.

Let your Users easily rate your Content. Simple to use Rating-Extension with Google Rating Rich Snippets. The Extension is based on the jQuery JavaScript Framework.',
    'category' => 'plugin',
    'version' => '3.0.1',
    'state' => 'stable',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => false,
    'author' => 'Kevin Lieser',
    'author_email' => 'info@ka-mediendesign.de',
    'author_company' => 'KA Mediendesign',
    'constraints' => [
        'depends' => [
            'extbase' => '7.6.0-10.4.99',
            'fluid' => '7.6.0-10.4.99',
            'typo3' => '7.6.0-10.4.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
