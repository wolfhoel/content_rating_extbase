<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'TYPO3.ContentRatingExtbase',
    'Contentrating',
    [
        'ContentRating' => 'show',
    ],
    // non-cacheable actions
    [
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'TYPO3.ContentRatingExtbase',
    'Ajaxrating',
    [
        'ContentRating' => 'rate',
    ],
    // non-cacheable actions
    [
    ]
);

// @todo Abfrage des EM und übergabe an Konstanten
call_user_func(
    function ($extKey, $_extconf) {
        // Extension configuration
        if ($_extconf) {
            $_extconf = unserialize($_extconf);
        } else {
            // default setting
            $_extconf = [
                'jquery_included' => '1',
                'style' => 'green.orange',
            ];
        }

        // Jquery
        if (array_key_exists('jquery_included', $_extconf) && '1' === $_extconf['jquery_included']) {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.jquery_included = 0');
        } else {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.jquery_included = 1');
        }

        // Style 1
        if (array_key_exists('style', $_extconf) && 'green.orange' === $_extconf['style']) {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.style = green.orange');
        }
        // Style 2
        elseif (array_key_exists('style', $_extconf) && 'yellow.orange' === $_extconf['style']) {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.style = yellow.orange');
        }
        // Style 3
        elseif (array_key_exists('style', $_extconf) && 'blue.gray' === $_extconf['style']) {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.style = blue.gray');
        }
        // Style 4
        elseif (array_key_exists('style', $_extconf) && 'pink' === $_extconf['style']) {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.style = pink');
        }
        // Style 5
        elseif (array_key_exists('style', $_extconf) && 'stars' === $_extconf['style']) {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.style = stars');
        } else {
            // Contstant
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.content_rating_extbase.style = green.orange');
        }
    },
    'content_rating_extbase',
    $_EXTCONF
);
