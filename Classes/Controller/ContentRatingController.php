<?php

namespace TYPO3\ContentRatingExtbase\Controller;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/*
 *  Copyright notice
 *
 *  (c) 2013
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 */

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ContentRatingController extends ActionController
{
    protected $hmacSalt = 'contentRatingExtbase_a3f60445f2031b5cd83534130eeba64cf4a0887b';

    /**
     * cObj.
     *
     * @return
     */
    private $cObj;

    private $output = '';

    private $url;
    private $urlhash;
    private $ip;
    private $getvars;
    private $pid;

    private $rate_count = 0;
    private $rate_value;
    private $rate_percent;

    private $rate_tmp1;
    private $rate_tmp2;
    private $rate_tmp3;

    /**
     * action list.
     */
    public function showAction()
    {
        // jQuery
        $confArray = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('content_rating_extbase');

        if (0 == $confArray['jquery_included']) {
            $includeJQuery = 1;
        } else {
            $includeJQuery = 0;
        }

        // Set Vars
        $this->url = $_SERVER['REQUEST_URI'];
        //if(GeneralUtility::_GP("url")) { $this->url = GeneralUtility::_GP("url"); }
        $this->urlhash = GeneralUtility::hmac($this->url, $this->hmacSalt);
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->pid = $GLOBALS["TSFE"]->id;

        // Fetch All Records
        $fromTable = 'tx_contentrating_rates';

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($fromTable);

        $recordList = [];
        $recordList = $queryBuilder
            ->select('*')
            ->from($fromTable)
            ->where(
                $queryBuilder->expr()->eq('rate_url', $queryBuilder->createNamedParameter($this->url, \PDO::PARAM_STR)),
                $queryBuilder->expr()->eq('t3_origuid', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute()
            ->fetchAll()
        ;

        $this->rate_count = count($recordList);
        foreach ($recordList as $record) {
            $this->rate_tmp1 = $this->rate_tmp1 + $record['rate_value'];
        }

        // Calculate
        $this->rate_tmp2 = $this->rate_count * 5; // Max possible Rating
        if (0 != $this->rate_tmp2) {
            $this->rate_tmp3 = round((100 / $this->rate_tmp2) * $this->rate_tmp1); // Percent Rating
        } else {
            $this->rate_tmp3 = 0; // Percent Rating
        }

        // Style
        if (empty($confArray['style']) || !isset($confArray['style'])) {
            $useStyle = 'green.orange';
        } else {
            $useStyle = $confArray['style'];
        }

        $styleCssFile = 'EXT:content_rating_extbase/Resources/Public/style.' . $useStyle . '.css';
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile($styleCssFile);

        // Assign...
        $this->view->assign('includeJQuery', $includeJQuery);
        $this->view->assign('useStyle', $useStyle);
        $this->view->assign('rateUrl', $this->url);
        $this->view->assign('rateUrlhash', $this->urlhash);
        $this->view->assign('ratePerc', $this->rate_tmp3);
        $this->view->assign('rateCount', $this->rate_count);
        $this->view->assign('rateValue', ((1 / 20) * $this->rate_tmp3));
    }

    public function rateAction()
    {
        $rateUrl = GeneralUtility::_GP('url');
        $rateUrlHash = GeneralUtility::_GP('urlhash');
        $checkUrlHash = GeneralUtility::hmac($rateUrl, $this->hmacSalt);

        if ($checkUrlHash == $rateUrlHash) {
            // Maximum Rate value
            $rateValue = (int)(GeneralUtility::_GP('rate'));
            if ($rateValue < 0) {
                $rateValue = 0;
            }
            if ($rateValue > 5) {
                $rateValue = 5;
            }

            // Check IP
            $fromTable = 'tx_contentrating_rates';
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($fromTable);

            $recordList = [];
            $recordList = $queryBuilder
                ->select('*')
                ->from($fromTable)
                ->where(
                    $queryBuilder->expr()->eq('rate_url', $queryBuilder->createNamedParameter($rateUrl, \PDO::PARAM_STR)),
                    $queryBuilder->expr()->eq('rate_ip', $queryBuilder->createNamedParameter($_SERVER['REMOTE_ADDR'], \PDO::PARAM_STR))
                )
                ->setMaxResults(1)
                ->execute()
                ->fetch()
            ;

            if (empty($recordList)) {
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($fromTable);
                $row = $queryBuilder
                    ->insert($fromTable)
                    ->values([
                        'pid' => 1,
                        'rate_url' => $rateUrl,
                        'rate_ip' => $_SERVER['REMOTE_ADDR'],
                        'rate_value' => $rateValue,
                        'tstamp' => time(),
                        'crdate' => time(),
                    ])
                    ->execute()
                ;
            } else {
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($fromTable);
                $queryBuilder
                    ->update($fromTable)
                    ->where(
                        $queryBuilder->expr()->eq('rate_url', $queryBuilder->createNamedParameter($rateUrl, \PDO::PARAM_STR)),
                        $queryBuilder->expr()->eq('rate_ip', $queryBuilder->createNamedParameter($_SERVER['REMOTE_ADDR'], \PDO::PARAM_STR))
                    )
                    ->set('rate_value', $rateValue)
                    ->set('tstamp', time())
                    ->execute()
                ;
            }

            // New Calc return
            $fromTable = 'tx_contentrating_rates';
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($fromTable);

            $recordList = [];
            $recordList = $queryBuilder
                ->select('*')
                ->from($fromTable)
                ->where(
                    $queryBuilder->expr()->eq('rate_url', $queryBuilder->createNamedParameter($rateUrl, \PDO::PARAM_STR))
                )
                ->execute()
                ->fetchAll()
            ;

            $this->rate_count = count($recordList);
            foreach ($recordList as $record) {
                $this->rate_tmp1 = $this->rate_tmp1 + $record['rate_value'];
            }

            // Calculate
            $this->rate_tmp2 = $this->rate_count * 5; // Max possible Rating
            $this->rate_tmp3 = round((100 / $this->rate_tmp2) * $this->rate_tmp1); // Percent Rating

            // Output
            echo $this->rate_tmp3;
            exit;
        }
    }
}
