<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_contentrating_rates'] = [
    'ctrl' => [
        'title' => 'LLL:EXT:content_rating_extbase/Resources/Private/Language/locallang_db.xlf:tx_contentratingextbase_domain_model_rates',
        'label' => 'rate_url',
    ],
    'interface' => [
    ],
    'types' => [
        '1' => ['showitem' => 'rate_url, rate_pid, rate_getvars, rate_ip, rate_value'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'rate_url' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:content_rating_extbase/Resources/Private/Language/locallang_db.xlf:tx_contentrating_rates.rateurl',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],
        'rate_pid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:content_rating_extbase/Resources/Private/Language/locallang_db.xlf:tx_contentrating_rates.ratepid',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],
        'rate_getvars' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:content_rating_extbase/Resources/Private/Language/locallang_db.xlf:tx_contentrating_rates.rategetvars',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],
        'rate_ip' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:content_rating_extbase/Resources/Private/Language/locallang_db.xlf:tx_contentrating_rates.rateip',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],
        'rate_value' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:content_rating_extbase/Resources/Private/Language/locallang_db.xlf:tx_contentrating_rates.ratevalue',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
            ],
        ],
    ],
];
